<?php

declare(strict_types=1);

namespace Drupal\http_request_mock;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\State\StateInterface;
use Drupal\http_request_mock\Attribute\ServiceMock;
use Psr\Http\Message\RequestInterface;

/**
 * Provides a default implementation for 'plugin.manager.service_mock' service.
 */
class ServiceMockPluginManager extends DefaultPluginManager implements ServiceMockPluginManagerInterface {

  /**
   * The state service.
   */
  protected StateInterface $state;

  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    StateInterface $state,
  ) {
    parent::__construct(
      'Plugin/ServiceMock',
      $namespaces,
      $moduleHandler,
      ServiceMockPluginInterface::class,
      ServiceMock::class,
      'Drupal\http_request_mock\Annotation\ServiceMock',
    );
    $this->alterInfo('service_mock_info');
    $this->setCacheBackend($cacheBackend, 'service_mock_plugins');
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function getMatchingPlugin(RequestInterface $request, array $options): ?ServiceMockPluginInterface {
    $allowed_plugins = $this->state->get('http_request_mock.allowed_plugins');
    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      // Allow tests to limit to a list of plugins.
      if (!$allowed_plugins || in_array($plugin_id, $allowed_plugins, TRUE)) {
        $plugin = $this->createInstance($plugin_id);
        assert($plugin instanceof ServiceMockPluginInterface);
        if ($plugin->applies($request, $options)) {
          return $plugin;
        }
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function findDefinitions(): array {
    $definitions = parent::findDefinitions();
    // Sort by weight. We're doing the sort in this method because is just
    // before the definitions are cached.
    uasort($definitions, [SortArray::class, 'sortByWeightElement']);
    return $definitions;
  }

}
