<?php

declare(strict_types=1);

namespace Drupal\http_request_mock\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a service mock plugin attribute object.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class ServiceMock extends Plugin {

  /**
   * Constructs a new plugin instance.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The translatable plugin label.
   * @param int $weight
   *   (optional) The weight. This determines the order in which plugin
   *   definitions are sorted.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $label,
    public readonly int $weight = 0,
  ) {}

}
