# HTTP Request Mock

[![https://drupalcode.org/project/http_request_mock/badges/1.x/pipeline.svg](https://drupalcode.org/project/http_request_mock/badges/1.x/pipeline.svg)](https://drupalcode.org/project/http_request_mock)

Inspired by Danny Sipos's (Upchuk) article:
https://www.webomelette.com/simple-guzzle-api-mocking-functional-testing-drupal-8

## Description

When running tests, you don't want to perform external HTTP requests while you
still want to test code that consumes such webservices. This module intercepts
the requests made by the Drupal `http_client` service and allows a plugin to
respond with a mocked response.

## Usage

In order to mock a webservice, you'll need to create a _service mock_ plugin.
Such a plugin implements the `ServiceMockPluginInterface`. The plugin manager
will pickup the first plugin that matches the HTTP request and will call the
plugin's `::getResponse()` method. Note that several plugins may qualify for the
same request, but you can specify a `weight` in plugin's annotation so that such
plugins are prioritized.

Implement one or more plugins for each webservice that you want to mock. In your
tests, enable this module, and the modules that are shipping such plugins. This
module ships a testing plugin that intercepts all outgoing HTTP requests made to
example.com.

In some cases, tests may narrow the list of plugins to a limited set. Such tests
should pass an array of plugin IDs to the `http_request_mock.allowed_plugins`
state variable. Leaving this variable empty or not set, will allow all plugins
to apply to the following outgoing HTTP requests.

## Contributing

[DDEV](https://ddev.com), a Docker-based PHP development tool for a streamlined
and unified development process, is the recommended tool for contributing to the
module. The [DDEV Drupal Contrib](https://github.com/ddev/ddev-drupal-contrib)
addon makes it easy to develop a Drupal module by offering the tools to set up
and test the module.

### Install DDEV

* Install a Docker provider by following DDEV [Docker Installation](https://ddev.readthedocs.io/en/stable/users/install/docker-installation/)
  instructions for your Operating System.
* [Install DDEV](https://ddev.readthedocs.io/en/stable/users/install/ddev-installation/),
  use the documentation that best fits your OS.
* DDEV is used mostly via CLI commands. [Configure shell completion &
  autocomplete](https://ddev.readthedocs.io/en/stable/users/install/shell-completion/)
  according to your environment.
* Configure your IDE to take advantage of the DDEV features. This is a critical
  step to be able to test and debug your module. Remember, the website runs
  inside Docker, so pay attention to these configurations:
    - [PhpStorm Setup](https://ddev.readthedocs.io/en/stable/users/install/phpstorm/)
    - [Configure](https://ddev.readthedocs.io/en/stable/users/debugging-profiling/step-debugging/)
      PhpStorm and VS Code for step debugging.
    - Profiling with [xhprof](https://ddev.readthedocs.io/en/stable/users/debugging-profiling/xhprof-profiling/),
      [Xdebug](https://ddev.readthedocs.io/en/stable/users/debugging-profiling/xdebug-profiling/)
      and [Blackfire](https://ddev.readthedocs.io/en/stable/users/debugging-profiling/blackfire-profiling/).

### Checkout the module

Normally, you check out the code form an [issue fork](https://www.drupal.org/docs/develop/git/using-gitlab-to-contribute-to-drupal/creating-issue-forks):

```shell
git clone git@git.drupal.org:issue/http_request_mock-[issue number].git
cd http_request_mock-[issue number]
```

### Start DDEV

Inside the cloned project run:

```shell
ddev start
```

This command will fire up the Docker containers and add all configurations.

### Install dependencies

```shell
ddev poser
```

This will install the PHP dependencies. Note that this is a replacement for
Composer _install_ command that knows how to bundle together Drupal core and the
module. Read more about this command at
https://github.com/ddev/ddev-drupal-contrib?tab=readme-ov-file#commands

```shell
ddev symlink-project
```

This symlinks the module inside `web/modules/custom`. Read more about this
command at https://github.com/ddev/ddev-drupal-contrib?tab=readme-ov-file#commands.
Note that as soon as `vendor/autoload.php` has been generated, this command runs
automatically on every `ddev start`.

This command should also be run when adding new directories or files to the root
of the module.

```shell
ddev exec "cd web/core && yarn install"
```

Install Node dependencies. This is needed for the `ddev eslint` command.

### Install Drupal

```shell
ddev install
```

This will install Drupal and will enable the module.

### Changing the Drupal core version

* Create a file `.ddev/config.local.yaml`
* In the new config file, set the desired Drupal core version. E.g.,
  ```yaml
  web_environment:
    - DRUPAL_CORE=^10.3
  ```
* Run `ddev restart`

Note that this file is not under VCS control. Refer to the original
documentation: [Changing the Drupal core version](https://github.com/ddev/ddev-drupal-contrib/blob/main/README.md#changing-the-drupal-core-version)

### Run tests

* `ddev phpunit`: run PHPUnit tests
* `ddev phpcs`: run PHP coding standards checks
* `ddev phpcbf`: fix coding standards findings
* `ddev phpstan`: run PHP static analysis
* `ddev eslint`: Run ESLint on YAML files.

## Maintainer

Claudiu Cristea | https://www.drupal.org/u/claudiucristea
